Introduction
Keywords: introduction, index, homepage, main, mainpage
Description: The main page of the help system
    Welcome to the prometheus MUSHclient soundpack.
With this help system, we don't aim to provide you any kind of support for playing the game itself, nor are we providing assistance in situations where you are stuck and can't play yourself out of said situations.
These help files are just meant to provide a reference for using this soundpack and it's special features.
If you need any help playing the game prometheus, ask in game for example on the newbie-channel or if you are really stuck,
please submit an assist so that a host can help you. Also reports and suggestions regarding the game, not the soundpack, need to be submitted utilizing the appropriate commands such as @report or @suggest.
For assistance using the soundpack, please use the \"soundpacks\"-channel.
If you have any suggestions, comments, or you encounter any bugs, please tune in your metafrequency to 140.8.
Want to see whats new? type "changelog".
You'd like to read the old readme? type "readme".
Want to know which help files are available? Type "shelp list".

On the following pages, you can find alot information about: